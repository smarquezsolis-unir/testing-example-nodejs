var expect    = require("chai").expect;
var calculadora = require("../calculadora");

describe("Calculadora Testing", function() {
  describe("Suma", function() {
    it("Suma los numeros 4 y 7", function() {
      var suma   = calculadora.suma(4, 7);

      expect(suma).to.equal(11);
    });
  });

  describe("Resta", function() {
    it("Resta los numeros 4 y 7", function() {
      var resta   = calculadora.resta(4, 7);

      expect(resta).to.equal(-3);
    });
  });

  describe("Multiplica", function() {
    it("Multiplica los numeros 4 y 7", function() {
      var multiplica   = calculadora.multiplica(4, 7);

      expect(multiplica).to.equal(28);
    });
  });

  describe("Divide", function() {
    it("Divide los numeros 4 y 7", function() {
      var divide   = calculadora.divide(8, 4);

      expect(divide).to.equals(2);
    });
  });





});
